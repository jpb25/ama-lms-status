import requests
import ujson
import time
from prettytable import PrettyTable

def main():
    print(f"{'='*25}\nQuery Your LMS Details\nCreated By: Juan Paolo B.\n{'='*25}\n")
    StudentID = input("Provide Your StudentID: ")
    SYTerm = input("Provide Your SYTerm (Default: 2213): ") or "2213"

    r = requests.get(f"https://discipulus.amasystem.net/LMSLogin/Requirements?StudentID={StudentID}&SYTerm={SYTerm}")
    data = ujson.loads(r.text)
    data_table = ujson.loads(data["data"])

    table = PrettyTable()
    table.field_names = ["Term", "ACAD Career / Subject", "Class Type", "Section", "Teacher", "Virtual", "F2F"]
    
    for x in data_table["Table"]:
        table.add_row([x["term"], (x["acadcareer"] + " " + x["subjectcode"]), (x["classcomponent"] + " " + x["ClassType"]), x["classnumber"], x["TeacherName"], x["isvcclass"], x["isF2F"]])
    
    print(table)
    
if __name__ == '__main__':
    main()
    input("Press ENTER to Exit...")
    time.sleep(3)